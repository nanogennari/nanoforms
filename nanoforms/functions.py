from flask import render_template
import re
import os
import time


def load_form(filename, string=False):
    Qs = []
    if string:
        data = filename
    else:
        with open(filename, 'r') as f:
            data = f.read()
    for line in data.split("\n"):
        line = line.strip()
        if line != "" and not line.startswith("#"):
            l, n, d, t, *e = line.split("|")
            Qs.append((l, n, d, t, e))
    return Qs


def render_form(filename):
    Qs = []
    for l, n, d, t, e in load_form(filename):
        Qs.append(render_template(f"inputs/{t}.html",label=l, name=n, desc=d, extras=e))
    return Qs


def write_form(str, form):
    with open(os.path.join(form, f"{time.time():.0f}.txt"), "w") as f:
        f.write(str)

def check_folder(folder):
    if not os.path.isdir(folder):
        os.mkdir(folder)

valre = lambda v, p: re.fullmatch("|".join(p), v)
valreq = lambda v, e: (v == "" and e[-1] != "req") or v != ""

VALIDATE = {
    "text": valre,
    "textarea": valre,
    "select": valreq,
    "checkbox": valreq,
    "radio": valreq,
    "radio-inline": valreq,
}

get_forms = lambda: [f.split(".")[0] for f in os.listdir() if f.endswith(".form")]
del_form = lambda f: os.remove(f"{f}.form")