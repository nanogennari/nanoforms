from . import app
from .functions import get_forms, load_form, VALIDATE, check_folder, render_form, del_form, write_form
from flask import render_template, redirect, request, flash


@app.route("/", methods=["GET"])
def index():
    forms = get_forms()
    return render_template("index.html", forms=forms)


@app.route("/form", methods=["GET"])
def form():
    form = request.args.get("form")
    return render_template("form.html", Qs=render_form(f"{form}.form"), form=form)

@app.route("/submit", methods=["POST"])
def submit():
    g = request.form.get
    form = g("form")
    fail = False
    fstr = ""
    for l, n, d, t, e in load_form(f"{form}.form"):
        v = g(n, "").strip()
        if t in VALIDATE:
            if VALIDATE[t](v, e):
                if v:
                    fstr += n + "\n|" + v.replace("\n|", "\n\\|") + "\n"
            else:
                fail = True
                flash(f"Campo {l}, preenchimento incorreto. {d}.")
    if not fail:
        check_folder(form)
        try:
            write_form(fstr, form)
        except:
            fail = True
            flash("Erro de escrita de formulário!")
    return render_template("feedback.html", fail=fail, form=form)

@app.route("/new", methods=["GET", "POST"])
def new_form_parse():
    if request.method == 'GET':
        form = request.args.get("form")
        if form:
            with open(f"{form}.form", 'r') as f:
                script = f.read()
            return render_template("new-form.html", name=form, script=script)
        else:
            return render_template("new-form.html")
    name = request.form.get("name")
    script = request.form.get("script")
    try:
        ls = [l for l, *_ in load_form(script, string=True)]
        fok = True
    except:
        flash("Script de formulário inválido.")
        fok = False
    if request.form.get("submit") == "Testar" or not fok:
        for l in ls:
            flash(l)
        return render_template("new-form.html", name=name, script=script, test=fok)
    else:
        if fok:
            with open(f"{name}.form", "w") as f:
                f.write(script)
            return redirect(f"./form?form={name}")

@app.route("/edit", methods=["GET"])
def edit():
    delete = request.args.get("delete")
    if delete:
        del_form(delete)
    forms = get_forms()
    return render_template("edit-forms.html", forms=forms)