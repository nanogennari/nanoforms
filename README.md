# nanoForms

A realy small forms aplication. Currently less than 200KB using Flask, to be ported in the future to picoweb.

![Example form](./form_example.png)

## Forms script

Formas are defined with a script where each line is a field:

```
[Field label]|[field_name]|[Field descryption]|[field_type]|[field parameters separeted by | ]
```

Available field types are:

* `text`
    * one parameter: a python regex to validade field, empty if field is not required.
* `textarea`
    * one parameter: a python regex to validade field, empty if field is not required.
* `checkbox`
    * one parameter: `req` if the field is required, empty if not.
* `radio`
    * n parameters for the n options in the selection.
    * last parameter: `req` if the field is required, empty if not.
* `radio-inline`
    * n parameters for the n options in the selection.
    * second to last parameter: one, two, three, ..., twelve, for the width of each item.
    * last parameter: `req` if the field is required, empty if not.
* `select`
    * n parameters for the n options in the selection.
    * last parameter: `req` if the field is required, empty if not.

We also have two organizers types (they don't colect data):

* `heading`
    * one parameter: `h1`, `h2`, `h3`, `h4`, `h5`, `h6`, for the heading format, defaults to `h4` if empty.
* `separator`
    * no parametrs, parameters section must be empty: `Separator|||separator|`

Examples regex for `text` and `textarea`:

* Not empty: `[\S\s]+[\S]+`
* E-mail: `^\S+@\S+\.\S+$`
* Date XX/XX/XXXX: `^\d{2}\/\d{2}\/\d{4}`

Se the `Formulário exemplo.form` file for examples usage for each field type.

In the future we intend to make available an online interactive forms gereration tool.

## Data storage

As a minimalist code, data is stored on **text files**, the app creates **one folder for each form** and a text file for each submition, in the future we intend to make available a tool to convert the text files to `csv`.